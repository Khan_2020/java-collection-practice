import java.util.Collections;
import java.util.List;

public class ReduceCollection {
    public static int getMax(List<Integer> list) {
        return list.stream().reduce(0, (sum, item) -> Math.max(sum, item));
    }

    public static double getAverage(List<Integer> list) {
        return list.stream().reduce(0, (a, b) -> a + b) / (double) list.size();
    }

    public static int getSum(List<Integer> list) {
        return list.stream().reduce(0, (a, b) -> a + b);
    }

    public static double getMedian(List<Integer> list) {
        return (double) list.get(list.size() / 2);
    }

    public static int getFirstEven(List<Integer> list) {
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i) % 2 == 0) return list.get(i);
        }
        return -1;
    }
}

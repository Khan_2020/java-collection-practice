import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;

public class FilterCollection {

    public static List<Integer> getAllEvens(List<Integer> list) {
        return list.stream().filter(item -> item % 2 == 0)
                .collect(Collectors.toList());
    }

    public static List<Integer> removeDuplicateElements(List<Integer> list) {
        // 未实现 return list.stream().filter(item -> ).collect(Collectors.toList());

        // 来回转换法
        return new ArrayList<>(new HashSet<Integer>(list));
    }

    public static List<Integer> getCommonElements(List<Integer> collection1, List<Integer> collection2) {
        return collection1.stream().filter(item -> collection2.contains(item)).collect(Collectors.toList());
    }
}

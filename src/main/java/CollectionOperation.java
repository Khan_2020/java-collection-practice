import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class CollectionOperation {

    public static List<Integer> getListByInterval(int left, int right) {
        List<Integer> l = new ArrayList<>();
        if (left <= right) {
            while (left <= right) {
                l.add(left);
                left++;
            }
        } else {
            while (left >= right) {
                l.add(left);
                left--;
            }
        }

        return l;
    }

    public static List<Integer> removeLastElement(List<Integer> list) {
        list.remove(list.size() - 1);
        return list;
    }

    public static List<Integer> sortDesc(List<Integer> list) {
        Collections.sort(list, (i1, i2) -> i2 - i1);
        return list;
    }


    public static List<Integer> reverseList(List<Integer> list) {
        Collections.reverse(list);
        return list;
    }


    public static List<Integer> concat(List<Integer> list1, List<Integer> list2) {
        list1.addAll(list2);
        return list1;
    }

    public static List<Integer> union(List<Integer> list1, List<Integer> list2) {
        Set<Integer> s = new HashSet<Integer>(list1);
        for (Integer i : list2) s.add(i);

        List<Integer> l = new ArrayList<>(s);
        return l;
    }

    public static boolean isAllElementsEqual(List<Integer> list1, List<Integer> list2) {
        if (list1.size() != list2.size())
            return false;

        Collections.sort(list1);
        Collections.sort(list2);

        for (int i = 0; i < list1.size(); i++) {
            if (list1.get(i) != list2.get(i))
                return false;
        }

        return true;
    }
}
